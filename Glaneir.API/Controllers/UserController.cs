﻿using Glaneir.API.Filters;
using Glaneir.Core.Enumerations;
using Glaneir.Core.Models;
using Glaneir.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Glaneir.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Users")]
    public class UserController : ApiController
    {
        private UserService _service;

        public UserController()
        {
            _service = new UserService();
        }

        [HttpGet]
        [Route("EditMyProfile")]
        public async Task<ResponseModel<object>> GetMyProfileForEdit()
        {
            ResponseModel<object> mResult = await _service.GetMyProfileForEdit(User.Identity.GetUserId());
            return mResult;
        }

        [HttpPost]
        [Route("SaveMyProfile")]
        public async Task<ResponseModel<object>> SaveMyProfile(EditMyProfileSumbitModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            model.UserId = User.Identity.GetUserId();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
            }
            mResult = await _service.SaveMyProfile(model);
            return mResult;
        }

        [HttpPost]
        [Route("ChangePassword")]
        public async Task<ResponseModel<object>> ChangePassword(UserChangePasswordModel model)
        {
            model.UserId = User.Identity.GetUserId();
            Validate(model);
            if (ModelState.IsValid)
            {
                return await _service.ChangePassword(model);
            }
            else
            {
                string Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                return new ResponseModel<object>() { Message = Message };
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                return await _service.ForgotPassword(model);
            }
            else
            {
                string Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                return new ResponseModel<object>() { Message = Message };
            }
        }

        [HttpPost]
        [Route("SaveContactUs")]
        public async Task<ResponseModel<object>> SaveContactUs(ContactUsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
            }

            mResult = await _service.SaveContact(model);
            return mResult;
        }

        [HttpPost]
        [Route("UpdatePhoneNumber")]
        public async Task<ResponseModel<object>> UpdatePhoneNumber(VerifyPhoneNumberModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            model.UserId = User.Identity.GetUserId();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            return await _service.UpdatePhoneNumber(model);
        }
    }
}
