﻿using Glaneir.API.Filters;
using Glaneir.Core.Enumerations;
using Glaneir.Core.Models;
using Glaneir.Service;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Glaneir.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Card")]
    public class CardController : ApiController
    {
        private CardService _service;

        public CardController()
        {
            _service = new CardService();
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseModel<object>> Add(CardSaveRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            model.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                mResult = await _service.Add(model);
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
            }
            return mResult;
        }

        [Route("List")]
        public async Task<ResponseModel<List<CardholderIdModel>>> GetCardIdList()
        {
            ResponseModel<List<CardholderIdModel>> mResult = await _service.GetCardIdList(User.Identity.GetUserId());
            return mResult;
        }

        [HttpPost]
        [Route("Detail")]
        public async Task<ResponseModel<CardDetailModel>> GetCardDetail(CardholderIdModel model)
        {
            ResponseModel<CardDetailModel> mResult = await _service.CardDetail(model);
            return mResult;
        }

        [HttpPost]
        [Route("GetStatement")]
        public async Task<ResponseModel<List<StatementsResultModel>>> GetStatement(StatementRequestModel model)
        {
            ResponseModel<List<StatementsResultModel>> mResult = await _service.GetStatement(model);
            return mResult;
        }

        [HttpPost]
        [Route("LockToggle")]
        public async Task<ResponseModel<ChangedStatusResponseModel>> LockToggle(CardChangeStatusModel model)
        {
            if (model.OldStatus == CardStatus.DepositOnly)
                model.NewStatus = CardStatus.Open;
            else
                model.NewStatus = CardStatus.DepositOnly;

            ResponseModel<ChangedStatusResponseModel> mResult = await _service.LockUnlock(model);
            return mResult;
        }


        [HttpPost]
        [Route("Block")]
        public async Task<ResponseModel<ChangedStatusResponseModel>> Block(CardChangeStatusModel model)
        {
            model.NewStatus = CardStatus.Lost;
            ResponseModel<ChangedStatusResponseModel> mResult = await _service.ChangeStatus(model);
            return mResult;
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseModel<ChangedStatusResponseModel>> Delete(CardChangeStatusModel model)
        {
            model.NewStatus = CardStatus.Closed;
            ResponseModel<ChangedStatusResponseModel> mResult = await _service.ChangeStatus(model);
            return mResult;
        }

        [HttpPost]
        [Route("PinRequest")]
        public async Task<ResponseModel<object>> PinRequest(CardholderIdModel model)
        {
            ResponseModel<object> mResult = await _service.PinRequest(model);
            return mResult;
        }

        [HttpPost]
        [Route("AddBalance")]
        public async Task<ResponseModel<object>> AddBalance(AddBalanceRequestModel model)
        {
            ResponseModel<object> mResult = await _service.AddBalance(model, User.Identity.GetUserId());
            return mResult;
        }

        [HttpPost]
        [Route("PaytoOtherCard")]
        public async Task<ResponseModel<object>> PaytoOtherCard(CardToCardRequestModel model)
        {
            ResponseModel<object> mResult = await _service.PaytoOtherCard(model, User.Identity.GetUserId());
            return mResult;
        }
    }
}
