﻿using Glaneir.API.Filters;
using Glaneir.Core.Enumerations;
using Glaneir.Core.Models;
using Glaneir.Service;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Glaneir.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Account")]
    public class AccountController : ApiController
    {
        private UserService _service;
        
        public AccountController()
        {
            _service = new UserService();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("FacebookLogin")]
        public async Task<ResponseModel<object>> FacebookLogin(UserFacebookSignInModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.FacebookLogin(model);
            return mResult;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("UserLogin")]
        public async Task<ResponseModel<object>> UserLoginWithEmail(UserSignInModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.UserLoginWithEmail(model);
            return mResult;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("UserSignUp")]
        public async Task<ResponseModel<object>> SignUp(UserSignUpModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            Validate(model);

            if (!ModelState.IsValid)
            {
                mResult.Status = ResponseStatus.Failed;
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                mResult.Message = messages;
                return mResult;
            }

            mResult = await _service.UserSighnUp(model);
            return mResult;
        }

    }
}
