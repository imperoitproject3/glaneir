﻿using Glaneir.API.Filters;
using Glaneir.Core.Helper;
using Glaneir.Core.Models;
using Glaneir.Service;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Glaneir.API.Controllers
{
    [CustomAuthorization]
    [RoutePrefix("API/Notification")]
    public class NotificationController : ApiController
    {
        private NotificationService _service;
        public NotificationController()
        {
            _service = new NotificationService();
        }
        
        [HttpPost]
        [AllowAnonymous]
        [Route("Send")]
        public async Task<ResponseModel<List<NotificationResultModel>>> NotificationSend(NotificationPayload model)
        {
            PushNotification pushNotification = new PushNotification();
            return await pushNotification.SendNotification(model);
        }

        [Route("List")]
        public async Task<ResponseModel<List<NotificationModel>>> GetList()
        {
            ResponseModel<List<NotificationModel>> mResult = await _service.GetNotification(User.Identity.GetUserId());
            return mResult;
        }
    }
}
