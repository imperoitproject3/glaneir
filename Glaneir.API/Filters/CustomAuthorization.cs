﻿using Glaneir.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Microsoft.AspNet.Identity;
using Glaneir.Core.Enumerations;
using Glaneir.Core.Models;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;

namespace Glaneir.API.Filters
{
    public class CustomAuthorization : AuthorizeAttribute
    {
        private bool SkipAuthorization(HttpActionContext actionContext)
        {
            Contract.Assert(actionContext != null);

            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                       || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            UserService _service = new UserService();
            base.OnAuthorization(actionContext);

            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;
            string UserId = principal.Identity.GetUserId();
            bool Skip = SkipAuthorization(actionContext);

            if (string.IsNullOrEmpty(UserId) && Skip == false)
            {
                ResponseModel<object> mResult = new ResponseModel<object>();
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = "You are unauthorized to access this resource.";
                actionContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
                };
            }
            else
            {
                if (Skip == false)
                {
                    bool isActive = _service.CheckUserIsActive(UserId);
                    if (!isActive)
                    {
                        ResponseModel<object> mResult = new ResponseModel<object>();
                        mResult.Status = ResponseStatus.Unauthorized;
                        mResult.Message = "You are blocked by admin!";
                        actionContext.Response = new HttpResponseMessage
                        {
                            StatusCode = HttpStatusCode.Unauthorized,
                            Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
                        };
                    }
                }
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult.Status = ResponseStatus.Unauthorized;
            mResult.Message = "You are unauthorized to access this resource.";
            actionContext.Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Unauthorized,
                Content = new ObjectContent(mResult.GetType(), mResult, new JsonMediaTypeFormatter())
            };
        }
    }
}