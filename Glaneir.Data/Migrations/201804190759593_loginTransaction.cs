namespace Glaneir.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class loginTransaction : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserLoginTransaction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        DeviceToken = c.String(nullable: false),
                        Platform = c.Int(nullable: false),
                        IsLogout = c.Boolean(nullable: false),
                        LoginDateTime = c.DateTime(nullable: false),
                        LogoutDateTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserLoginTransaction", "UserId", "dbo.ApplicationUser");
            DropIndex("dbo.UserLoginTransaction", new[] { "UserId" });
            DropTable("dbo.UserLoginTransaction");
        }
    }
}
