﻿using Glaneir.Core.Enumerations;
using Glaneir.Data.Entities;
using Glaneir.Data.Helper;
using Glaneir.Data.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glaneir.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            var roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));
            var userManager = new ApplicationUserManager(new ApplicationStore(context));

            #region Insert Roles if not exist

            string roleName = Role.MobileUser.ToString();
            var role = roleManager.FindByNameAsync(roleName).Result;
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.CreateAsync(role).Result;
            }

            roleName = Role.Admin.ToString();
            role = roleManager.FindByNameAsync(roleName).Result;
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.CreateAsync(role).Result;
            }
            #endregion

            #region Add Admin User
            const string emailId = "admin@Glaneir.com";
            const string userName = "Admin";
            const string password = "Glaneir@123";
            var user = userManager.FindByNameAsync(userName).Result;
            if (user == null)
            {
                user = new ApplicationUser() { UserName = userName, Email = emailId,};

                var result = userManager.CreateAsync(user, password).Result;
                result = userManager.SetLockoutEnabledAsync(user.Id, false).Result;
            }
            #endregion

            #region Add Admin Role to Admin User
            var rolesForUser = userManager.GetRolesAsync(user.Id).Result;
            if (!rolesForUser.Contains(Role.Admin.ToString()))
            {
                var result = userManager.AddToRoleAsync(user.Id, Role.Admin.ToString()).Result;
            }
            #endregion
        }
    }
}
