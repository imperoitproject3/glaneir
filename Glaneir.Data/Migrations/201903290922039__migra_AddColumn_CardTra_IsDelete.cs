namespace Glaneir.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _migra_AddColumn_CardTra_IsDelete : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserCardTransactions", "IsDelete", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserCardTransactions", "IsDelete");
        }
    }
}
