namespace Glaneir.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransacationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserCardTransactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        CardName = c.String(nullable: false),
                        CardNumber = c.String(nullable: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserCardTransactions", "UserId", "dbo.ApplicationUser");
            DropIndex("dbo.UserCardTransactions", new[] { "UserId" });
            DropTable("dbo.UserCardTransactions");
        }
    }
}
