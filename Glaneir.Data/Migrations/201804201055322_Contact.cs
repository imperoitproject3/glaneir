namespace Glaneir.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContactUs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, unicode: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContactUs", "UserId", "dbo.ApplicationUser");
            DropIndex("dbo.ContactUs", new[] { "UserId" });
            DropTable("dbo.ContactUs");
        }
    }
}
