﻿using Glaneir.Data.Helper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace Glaneir.Data.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public bool IsActive { get; set; } = true;

        public DateTime CreatedDate { get; set; } = Utility.GetSystemDateTimeUTC();

        public virtual ICollection<UserLoginTransaction> LoginTransaction { get; set; }
        public virtual ICollection<UserCardTransactions> CardTransactions { get; set; }
        public virtual ICollection<UserNotification> UserNotifications { get; set; }
    }
}
