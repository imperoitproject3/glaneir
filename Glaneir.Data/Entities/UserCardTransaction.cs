﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Glaneir.Data.Entities
{
    public class UserCardTransactions : EntityBase
    {
        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [Required]
        public string CardName { get; set; }

        [Required]
        public string Cardholderid { get; set; }

        [Required]
        public string CardNumber { get; set; }

        public bool IsDelete { get; set; }
    }
}
