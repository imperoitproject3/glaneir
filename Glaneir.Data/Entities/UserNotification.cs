﻿using Glaneir.Data.Helper;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Glaneir.Data.Entities
{
    public class UserNotification
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [Required]
        [Column(TypeName = "Varchar(max)")]
        public string NotificationText { get; set; }

        public string ServerResponse { get; set; }

        [Required]
        public DateTime CreatedDateTimeUTC { get; set; } = Utility.GetSystemDateTimeUTC();
    }
}
