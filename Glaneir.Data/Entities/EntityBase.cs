﻿using Glaneir.Data.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glaneir.Data.Entities
{
    public class EntityBase
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public DateTime CreatedDateTimeUTC { get; set; } = Utility.GetSystemDateTimeUTC();

        public DateTime? UpdatedDateTimeUTC { get; set; }
    }
}
