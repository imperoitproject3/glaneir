﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Helper;
using Glaneir.Core.Models;
using Glaneir.Data.Entities;
using Glaneir.Data.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glaneir.Data.Repository
{
    public class CardRepository
    {
        private readonly ApplicationDbContext _ctx;

        public CardRepository()
        {
            _ctx = new ApplicationDbContext();
        }
        public async Task<ResponseModel<object>> Add(CardSaveRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (model.CardNumber != null)
            {
                //check card number avaibility 
                if (_ctx.UserCardTransactions.Any(x => x.CardNumber == model.CardNumber))
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Another card available same to this card number";
                }
                else
                {
                    string Cardholderid = string.Empty;

                    ////Create XML for Api request////
                    StringBuilder sbXML = new StringBuilder();
                    sbXML.Append("<GetCardID>");
                    sbXML.Append($"<Cardnumber>{model.CardNumber}</Cardnumber>");
                    sbXML.Append("</GetCardID>");

                    ///Api Httpclient request////
                    ResponseModel<PfsResultModel> PfResult = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
                    {
                        apiSigniture = "GetCardID",
                        httpMethodType = HttpMethodTypes.post,
                        xmlData = sbXML.ToString()
                    });

                    if (PfResult.Status == ResponseStatus.Success)
                    {
                        ////jsonString to Object
                        PFSGetCardResultModel result = JsonConvert.DeserializeObject<PFSGetCardResultModel>(PfResult.Result.jsonString);

                        Cardholderid = result.GetCardID.Cardholderid;

                        ////check Card status
                        ResponseModel<CardDetailModel> CardDetailsResult = await CardDetail(new CardholderIdModel { Cardholderid = Cardholderid });

                        if (CardDetailsResult.Status == ResponseStatus.Success)
                        {
                            CardStatus cSatus = CardDetailsResult.Result.CardStatus;

                            ///set card status
                            if (cSatus == CardStatus.Issued || cSatus == CardStatus.DepositOnly || cSatus == CardStatus.Closed)
                            {
                                ResponseModel<ChangedStatusResponseModel> ChangeStatusResponse = await ChangeStatus(new CardChangeStatusModel { Cardholderid = Cardholderid, OldStatus = cSatus, NewStatus = CardStatus.Open });
                            }
                        }
                    }
                    else
                    {
                        ApplicationUser User = _ctx.Users.Where(x => x.Id == model.UserId).FirstOrDefault();
                        List<string> name = User.FirstName.Trim().Split(' ').ToList();
                        string FirstName = string.Empty;
                        string LastName = string.Empty;
                        if (name.Count > 1)
                        {
                            FirstName = name[0];
                            LastName = name[1];
                        }
                        else
                        {
                            if (User.FirstName.Length <= 13)
                            {
                                FirstName = User.FirstName;
                                LastName = User.FirstName;
                            }
                            else
                            {
                                FirstName = User.FirstName.Substring(0, 12);
                                LastName = User.FirstName.Substring(13);
                            }
                        }

                        StringBuilder xmlData = new StringBuilder();
                        xmlData.Append("<CardIssue>");
                        xmlData.Append($"<FirstName>{model.CardName}</FirstName>");
                        xmlData.Append($"<LastName>{LastName}</LastName>");
                        xmlData.Append($"<Phone>{User.PhoneNumber}</Phone>");
                        xmlData.Append($"<Email>{User.Email}</Email>");
                        xmlData.Append($"<Address1>Fake address</Address1>");
                        xmlData.Append($"<City>Dublin</City>");
                        xmlData.Append($"<ZipCode>94568</ZipCode>");
                        xmlData.Append($"<CountryCode>IE</CountryCode>");
                        xmlData.Append($"<DistributorCode>4372</DistributorCode>");
                        xmlData.Append($"<CardStyle>01</CardStyle>");
                        xmlData.Append($"<DOB>27/3/1990</DOB>");
                        xmlData.Append($"<DeliveryType>VC</DeliveryType>");
                        xmlData.Append($"<bin>59991198</bin>");
                        xmlData.Append($"<VerifySSNOverride>Y</VerifySSNOverride>");
                        xmlData.Append($"<VerifyDOBOverride>Y</VerifyDOBOverride>");
                        xmlData.Append($"<GeoIPCheckOverride>Y</GeoIPCheckOverride>");
                        xmlData.Append("</CardIssue>");

                        ResponseModel<PfsResultModel> PfsResult = await PrepaidFinancialServicesAPI.ApiService(
                            new PfsAPIRequestModel()
                            {
                                apiSigniture = "CardIssue",
                                httpMethodType = HttpMethodTypes.post,
                                xmlData = xmlData.ToString()
                            });

                        if (PfsResult.Status == ResponseStatus.Success)
                        {
                            PFSCardIssueReadModel result = JsonConvert.DeserializeObject<PFSCardIssueReadModel>(PfsResult.Result.jsonString);

                            Cardholderid = result.AccountAPIv2.CardIssue.Cardholderid;

                            ResponseModel<ChangedStatusResponseModel> ChangeStatusResponse = await ChangeStatus(new CardChangeStatusModel { Cardholderid = Cardholderid, OldStatus = CardStatus.Issued, NewStatus = CardStatus.Open });
                        }

                        mResult.Status = PfsResult.Status;
                        mResult.Message = PfsResult.Message;
                    }

                    if (!string.IsNullOrEmpty(Cardholderid))
                    {
                        UserCardTransactions entity = new UserCardTransactions()
                        {
                            CardName = model.CardName,
                            CardNumber = model.CardNumber,
                            UserId = model.UserId,
                            Cardholderid = Cardholderid
                        };

                        _ctx.UserCardTransactions.Add(entity);
                        await _ctx.SaveChangesAsync();

                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Your card is saved.";
                    }
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Card number is required";
            }
            return mResult;
        }

        public async Task<ResponseModel<List<CardholderIdModel>>> GetCardIdList(string UserId)
        {
            ResponseModel<List<CardholderIdModel>> mResult = new ResponseModel<List<CardholderIdModel>>();
            mResult.Result = await (from card in _ctx.UserCardTransactions
                                    where card.UserId == UserId && card.IsDelete == false
                                    orderby card.CreatedDateTimeUTC descending
                                    select new CardholderIdModel
                                    {
                                        Cardholderid = card.Cardholderid,
                                    }).ToListAsync();

            if (mResult.Result.Any())
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = $"Count: {mResult.Result.Count()}";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No card added.";
            }
            return mResult;
        }

        public async Task<ResponseModel<CardDetailModel>> CardDetail(CardholderIdModel model)
        {
            var todayNowUtc = Utility.GetSystemDateTimeUTC();
            ResponseModel<CardDetailModel> mResult = new ResponseModel<CardDetailModel>();

            ResponseModel<List<StatementsResultModel>> Statements = new ResponseModel<List<StatementsResultModel>>();

            StringBuilder GetInquiryXml = new StringBuilder();
            GetInquiryXml.Append("<CardInquiry>");
            GetInquiryXml.Append($"<Cardholderid>{model.Cardholderid}</Cardholderid>");
            GetInquiryXml.Append("</CardInquiry>");

            ResponseModel<PfsResultModel> CardInquiryResult = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
            {
                apiSigniture = "CardInquiry",
                httpMethodType = HttpMethodTypes.post,
                xmlData = GetInquiryXml.ToString()//
            });

            if (CardInquiryResult.Status == ResponseStatus.Success)
            {
                PFSCardReadModel cardResult = JsonConvert.DeserializeObject<PFSCardReadModel>(CardInquiryResult.Result.jsonString);

                if (cardResult != null && cardResult.AccountAPIv2 != null && cardResult.AccountAPIv2.CardInquiry != null && cardResult.AccountAPIv2.CardInquiry.cardholder != null && cardResult.AccountAPIv2.CardInquiry.cardinfo != null)
                {
                    CardInfoModel cardinfo = cardResult.AccountAPIv2.CardInquiry.cardinfo;
                    CardHolderModel cardholder = cardResult.AccountAPIv2.CardInquiry.cardholder;

                    CardStatus CardStatus = CardStatus.Issued;

                    switch (cardinfo.CardStatus)
                    {
                        case "0":
                            CardStatus = CardStatus.Issued;
                            break;
                        case "1":
                            CardStatus = CardStatus.Open;
                            break;
                        case "2":
                            CardStatus = CardStatus.Lost;
                            break;
                        case "3":
                            CardStatus = CardStatus.Stolen;
                            break;
                        case "4":
                            CardStatus = CardStatus.DepositOnly;
                            break;
                        case "5":
                            CardStatus = CardStatus.CheckReason;
                            break;
                        case "6":
                            CardStatus = CardStatus.Closed;
                            break;
                        case "A":
                            CardStatus = CardStatus.A_PinChangeRequired;
                            break;
                        case "C":
                            CardStatus = CardStatus.C_PhoneNumberVerification;
                            break;
                    }


                    mResult.Result = new CardDetailModel()
                    {
                        AvailableBalance = cardinfo.AvailBal,
                        Cardname = cardholder.FirstName,
                        CardNumber = cardholder.CardNumber,
                        CardStatus = CardStatus,
                        CardType = cardinfo.CardType,
                        Expirydate = cardinfo.ExpirationDate,
                        Cardholderid = model.Cardholderid
                    };
                }
                StatementRequestModel statementModel = new StatementRequestModel()
                {
                    Cardholderid = model.Cardholderid,

                    StartDate = todayNowUtc.AddDays(-6).Date,
                    //StartDate = DateTime.Parse("01-01-2019"),
                    EndDate = todayNowUtc.Date
                };

                Statements = await GetStatement(statementModel);

                if (Statements.Status == ResponseStatus.Success)
                {
                    mResult.Result.TransactionList = Statements.Result;
                    mResult.Message = Statements.Message;
                }
                else
                {
                    mResult.Message = Statements.Message;
                }

                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
            }
            //if (mResult.Result.TransactionList.Count == 0)
            //{
            //    mResult.Message = "Transaction not available";
            //}
            return mResult;
        }

        public async Task<ResponseModel<List<StatementsResultModel>>> GetStatement(StatementRequestModel model)
        {
            ResponseModel<List<StatementsResultModel>> mResult = new ResponseModel<List<StatementsResultModel>>();

            StringBuilder sbXML = new StringBuilder();
            sbXML.Append("<ViewStatement>");
            sbXML.Append($"<Cardholderid>{model.Cardholderid}</Cardholderid>");
            sbXML.Append($"<StartDate>{model.StartDate}</StartDate>");
            sbXML.Append($"<EndDate>{model.EndDate}</EndDate>");
            sbXML.Append("</ViewStatement>");

            ResponseModel<PfsResultModel> CardStatementResult = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
            {
                apiSigniture = "ViewStatement",
                httpMethodType = HttpMethodTypes.post,
                xmlData = sbXML.ToString()
            });

            if (CardStatementResult.Status == ResponseStatus.Success)
            {
                try
                {
                    PFSStatementReadModel statementResult = JsonConvert.DeserializeObject<PFSStatementReadModel>(CardStatementResult.Result.jsonString);

                    Cardholderstatementdetails StatementDetail = statementResult?.AccountApIv2?.ViewStatement?.Cardholderstatementdetails;
                    if (StatementDetail.RecCnt > 0)
                    {
                        foreach (var cardaccount in StatementDetail.Cardpan.Cardaccount)
                        {
                            string jsonDictionary = JsonConvert.SerializeObject(cardaccount.Transactionlist.Transaction);

                            Transaction Transaction = JsonConvert.DeserializeObject<Transaction>(jsonDictionary);

                            mResult.Result.Add(new StatementsResultModel { Date = Transaction.date, Description = Transaction.description, Amount = Transaction.amount, TransactionType = Transaction.TransactionType });
                        }
                    }
                }
                catch (Exception ex)
                {
                    SinglePFSStatementReadModel statementResult = JsonConvert.DeserializeObject<SinglePFSStatementReadModel>(CardStatementResult.Result.jsonString);

                    SingleCardholderstatementdetails StatementDetail = statementResult.AccountApIv2.ViewStatement.Cardholderstatementdetails;

                    if (StatementDetail.RecCnt > 0)
                    {
                        if (StatementDetail?.Cardpan?.Cardaccount?.Transactionlist != null)
                        {
                            string jsonDictionary = JsonConvert.SerializeObject(StatementDetail.Cardpan.Cardaccount.Transactionlist);

                            ViewTransaction Transaction = JsonConvert.DeserializeObject<ViewTransaction>(jsonDictionary);

                            mResult.Result.Add(new StatementsResultModel
                            {
                                Date = Transaction.Transaction.Date,
                                Description = Transaction.Transaction.Description,
                                Amount = Transaction.Transaction.Amount,
                                TransactionType = Transaction.Transaction.TransactionType
                            });
                        }
                    }
                }
                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = CardStatementResult.Message;
            }

            if (mResult.Result.Count == 0)
            {
                mResult.Message = "No transaction available";
            }
            return mResult;
        }

        public IQueryable<CardViewModel> GetUserCardList(string UserId)
        {
            return (from card in _ctx.UserCardTransactions
                    where card.UserId == UserId
                    select new CardViewModel
                    {
                        CardName = card.CardName,
                        CardNumber = card.CardNumber,
                        UserName = card.User.UserName,
                        CreatedDateTimeUTC = card.CreatedDateTimeUTC
                    }).OrderByDescending(x => x.CreatedDateTimeUTC);
        }

        public async Task<ResponseModel<ChangedStatusResponseModel>> LockUnlock(CardChangeStatusModel model)
        {
            ResponseModel<ChangedStatusResponseModel> mResult = new ResponseModel<ChangedStatusResponseModel>();

            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<LockUnlock>");
            xmlData.Append($"<Cardholderid>{model.Cardholderid}</Cardholderid>");
            xmlData.Append($"<OldStatus>{Convert.ToInt32(model.OldStatus)}</OldStatus>");
            xmlData.Append($"<NewStatus>{Convert.ToInt32(model.NewStatus)}</NewStatus>");
            xmlData.Append("</LockUnlock>");

            ResponseModel<PfsResultModel> pfsResult = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
            {
                apiSigniture = "LockUnlock",
                httpMethodType = HttpMethodTypes.post,
                xmlData = xmlData.ToString()
            });
            if (pfsResult.Status == ResponseStatus.Success)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = pfsResult.Message;
                mResult.Result = new ChangedStatusResponseModel { CardStatus = model.NewStatus };
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = pfsResult.Message;
                mResult.Result = new ChangedStatusResponseModel { CardStatus = model.OldStatus };
            }

            return mResult;
        }

        public async Task<ResponseModel<ChangedStatusResponseModel>> ChangeStatus(CardChangeStatusModel model)
        {
            ResponseModel<ChangedStatusResponseModel> mResult = new ResponseModel<ChangedStatusResponseModel>();

            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<ChangeCardStatus>");
            xmlData.Append($"<Cardholderid>{model.Cardholderid}</Cardholderid>");
            xmlData.Append($"<OldStatus>{Convert.ToInt32(model.OldStatus)}</OldStatus>");
            xmlData.Append($"<NewStatus>{Convert.ToInt32(model.NewStatus)}</NewStatus>");
            xmlData.Append("</ChangeCardStatus>");

            ResponseModel<PfsResultModel> CardLockResponse = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
            {
                apiSigniture = "ChangeCardStatus",
                httpMethodType = HttpMethodTypes.post,
                xmlData = xmlData.ToString()
            });

            if (model.NewStatus == CardStatus.Closed)
            {
                await SetCardDelete(model.Cardholderid);
            }

            if (model.NewStatus == CardStatus.Lost)
                mResult.Message = "Card blocked successfully";
            else
                mResult.Message = "Card deleted successfully";

            if (CardLockResponse.Status == ResponseStatus.Success)
            {
                mResult.Status = ResponseStatus.Success;
                //mResult.Message = CardLockResponse.Message;
                mResult.Result = new ChangedStatusResponseModel { CardStatus = model.NewStatus };
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                //mResult.Message = CardLockResponse.Message;
                mResult.Result = new ChangedStatusResponseModel { CardStatus = model.NewStatus };
            }
            return mResult;
        }

        public async Task<bool> SetCardDelete(string CardholderId)
        {
            UserCardTransactions ObjCard = await _ctx.UserCardTransactions.Where(x => x.Cardholderid == CardholderId).FirstOrDefaultAsync();
            bool istrue = false;
            if (ObjCard != null)
            {
                ObjCard.IsDelete = true;
                await _ctx.SaveChangesAsync();
                istrue = true;
            }
            return istrue;
        }

        public async Task<ResponseModel<object>> PinRequest(CardholderIdModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<A45>");
            xmlData.Append($"<Cardholderid>{model.Cardholderid}</Cardholderid>");
            xmlData.Append("</A45>");

            ResponseModel<PfsResultModel> PinRequestResponse = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
            {
                apiSigniture = "PinReminder",
                httpMethodType = HttpMethodTypes.post,
                xmlData = xmlData.ToString()
            });
            if (PinRequestResponse.Status == ResponseStatus.Success)
            {
                mResult.Status = ResponseStatus.Success;
                //mResult.Message = PinRequestResponse.Message;
                mResult.Message = "Your request for pin has been sent successfully";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                //mResult.Message = PinRequestResponse.Message;
                mResult.Message = "Your request for pin has been sent successfully";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AddBalance(AddBalanceRequestModel model, string userId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            if (string.IsNullOrEmpty(model.Description))
            {
                model.Description = "Deposit To Card API";
            }
            else if (model.Description.Length > 30)
            {
                model.Description = model.Description.Substring(30);
            }

            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<DepositToCard>");
            xmlData.Append($"<Cardholderid>{model.Cardholderid}</Cardholderid>");
            xmlData.Append($"<Amount>{model.Amount}</Amount>");
            xmlData.Append($"<TransactionType>{(int)TransactionTypes.Credit}</TransactionType>");
            xmlData.Append("<CurrencyCode>EUR</CurrencyCode>");
            xmlData.Append("<SettlementCurrencyCode>EUR</SettlementCurrencyCode>");
            xmlData.Append($"<TransactionDescription>{model.Description}</TransactionDescription>");
            xmlData.Append("</DepositToCard>");

            ResponseModel<PfsResultModel> pfsResult = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
            {
                apiSigniture = "DepositToCard",
                httpMethodType = HttpMethodTypes.post,
                xmlData = xmlData.ToString()
            });

            if (pfsResult.Status == ResponseStatus.Success)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Amount added successfully to your card";

                using (NotificationRepository repo = new NotificationRepository())
                {
                    await repo.Add(new NotificationAddModel()
                    {
                        NotificationText = mResult.Message,
                        UserId = userId
                    });
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = pfsResult.Message;
            }
            //mResult.Message = pfsResult.Message;
            return mResult;
        }

        public async Task<ResponseModel<object>> PaytoOtherCard(CardToCardRequestModel model, string userId)
        {

            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == userId);

            UserCardTransactions cardData = await _ctx.UserCardTransactions.FirstOrDefaultAsync(x => x.CardNumber == model.CardNumber.Trim());

            if (cardData != null)
            {
                StringBuilder xmlData = new StringBuilder();
                xmlData.Append("<Cardtocard>");
                xmlData.Append($"<Cardholderid>{model.Cardholderid}</Cardholderid>");
                xmlData.Append($"<Amount>{model.Amount}</Amount>");
                xmlData.Append("<CurrencyCode>EUR</CurrencyCode>");
                xmlData.Append($"<CardholderidTo>{cardData.Cardholderid}</CardholderidTo>");
                xmlData.Append($"<TerminalOwner>{user.FirstName}</TerminalOwner>");
                xmlData.Append("<TerminalLocation>B1 301 westgate</TerminalLocation>");
                xmlData.Append("<TerminalCity>Dublin</TerminalCity>");
                xmlData.Append("<TerminalState>IR</TerminalState>");
                xmlData.Append("<TerminalID>4258</TerminalID>");
                xmlData.Append("<Country>ie</Country>");
                xmlData.Append($"<Description>{model.Description}</Description>");
                xmlData.Append("<SettlementCurrencyCode>EUR</SettlementCurrencyCode>");
                xmlData.Append("<DirectFee></DirectFee>");
                xmlData.Append("</Cardtocard>");

                ResponseModel<PfsResultModel> pfsResult = await PrepaidFinancialServicesAPI.ApiService(new PfsAPIRequestModel()
                {
                    apiSigniture = "CardToCard",
                    httpMethodType = HttpMethodTypes.post,
                    xmlData = xmlData.ToString()
                });

                if (pfsResult.Status == ResponseStatus.Success)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Payment Successfully";

                    using (NotificationRepository repo = new NotificationRepository())
                    {
                        await repo.Add(new NotificationAddModel()
                        {
                            NotificationText = $"€{model.Amount} is debited to card no: {model.CardNumber} on {DateTime.UtcNow.ToString()}",
                            UserId = userId
                        });

                        await repo.Add(new NotificationAddModel()
                        {
                            NotificationText = $"€{model.Amount} is credited to card no: {cardData.CardNumber} on {DateTime.UtcNow.ToString()}",
                            UserId = cardData.UserId
                        });
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = pfsResult.Message;
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Card number not exist in the system.";
            }
            return mResult;
        }
    }
}
