﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Helper;
using Glaneir.Core.Models;
using Glaneir.Data.Entities;
using Glaneir.Data.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Glaneir.Data.Repository
{
    public class NotificationRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        public NotificationRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task Add(NotificationAddModel model)
        {
            ApplicationUser user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == model.UserId);

            string ServerResponse = string.Empty;
            if (user.LoginTransaction.Any())
            {
                List<string> devicetokenList = user.LoginTransaction.Select(x => x.DeviceToken).ToList();

                PushNotification pushNotification = new PushNotification();
                ResponseModel<List<NotificationResultModel>> notificationResult = await pushNotification.SendNotification(new NotificationPayload()
                {
                    data = new CustomDataModel()
                    {
                        primaryId = 0,
                    },
                    notification = new NotificationOptionModel()
                    {
                        body = model.NotificationText,
                        badge = 1
                    },
                    registration_ids = devicetokenList.ToArray(),
                });

                ServerResponse = JsonConvert.SerializeObject(notificationResult);
            }

            UserNotification entity = new UserNotification()
            {
                NotificationText = model.NotificationText,
                UserId = model.UserId,
                ServerResponse = ServerResponse,
            };
            _ctx.UserNotification.Add(entity);
            await _ctx.SaveChangesAsync();
        }

        public async Task<ResponseModel<List<NotificationModel>>> GetNotification(string Userid)
        {
            ResponseModel<List<NotificationModel>> mResult = new ResponseModel<List<NotificationModel>>();

            var Result = await (from x in _ctx.UserNotification
                                where x.UserId == Userid
                                orderby x.CreatedDateTimeUTC descending
                                select new
                                {
                                    x.CreatedDateTimeUTC,
                                    //HourAgo = x.CreatedDateTimeUTC.Day > DateTime.UtcNow.Day ? x.CreatedDateTimeUTC.ToString() : Utilities.TimeAgo(x.CreatedDateTimeUTC),
                                    x.NotificationText
                                }).ToListAsync();
            if (Result != null)
            {
                foreach (var item in Result)
                {
                    mResult.Result.Add(new NotificationModel
                    {
                        Text = item.NotificationText,
                        HourAgo = item.CreatedDateTimeUTC.Day > DateTime.UtcNow.Day ? item.CreatedDateTimeUTC.ToShortDateString() : Utilities.TimeAgo(item.CreatedDateTimeUTC)
                    });
                }
            }
            mResult.Status = ResponseStatus.Success;
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
