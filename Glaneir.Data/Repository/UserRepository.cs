﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Helper;
using Glaneir.Core.Models;
using Glaneir.Data.Entities;
using Glaneir.Data.Helper;
using Glaneir.Data.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Glaneir.Data.Repository
{
    public class UserRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        private readonly ApplicationUserManager _userManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IOwinContext _owin;

        public UserRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx));
        }

        public UserRepository(IOwinContext owin)
        {
            _owin = owin;
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx));
            _userManager.EmailService = new EmailService();
            _authenticationManager = owin.Authentication;
        }

        #region AdminPanel
        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByEmailAsync(model.EmailId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid username.";
            }
            else
            {
                var validRole = await _userManager.IsInRoleAsync(user.Id, model.Role.ToString());
                var validPassword = await _userManager.CheckPasswordAsync(user, model.Password);

                if (validRole)
                {
                    if (validPassword)
                    {
                        await SighnInAsync(user, false, model.RememberMe);
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = string.Format("User {0} Logged in successfully!", user.UserName);
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Invalid access.";
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            var userName = _owin.Authentication.User.Identity.Name;
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid username or password.";
            }
            else
            {
                IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Password changed successfully";
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Invalid password.";
                }
            }
            return mResult;
        }

        private async Task SighnInAsync(ApplicationUser user, bool isPersistent, bool rememberBrowser)
        {
            var userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            _authenticationManager.SignIn(
                new AuthenticationProperties { IsPersistent = isPersistent },
                userIdentity);
        }

        public void WebLogout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
               DefaultAuthenticationTypes.TwoFactorCookie);
        }

        public IQueryable<UserDetailsModel> getAllUsers_IQueryable()
        {
            return (from u in _ctx.Users
                    from r in u.Roles
                    join role in _ctx.Roles on r.RoleId equals role.Id
                    where role.Name != Role.Admin.ToString()
                    select new UserDetailsModel
                    {
                        UserId = u.Id,
                        Name = u.FirstName,
                        Email = u.Email,
                        IsVerifyed = u.IsActive,
                        CreatedDate = u.CreatedDate,
                        NumberOFCard = u.CardTransactions.Count(),
                        PhoneNumber = u.PhoneNumber
                    }).OrderByDescending(x => x.CreatedDate);
        }

        public ResponseModel<bool> ActiveToggle(string userId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            var user = _ctx.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                user.IsActive = !user.IsActive;
                _ctx.SaveChanges();

                mResult.Status = ResponseStatus.Success;
                if (user.IsActive)
                {
                    mResult.Message = "Activated successfully.";
                }
                else
                {
                    mResult.Message = "Deactivated successfully.";
                }
                mResult.Result = user.IsActive;
            }
            else
            {
                mResult.Message = "User not found.";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> Delete(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            var user = _ctx.Users.FirstOrDefault(p => p.Id == UserId);
            if (user != null)
            {
                _ctx.Users.Remove(user);
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "User deleted successfully.";
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }
        #endregion

        public bool CheckUserIsActive(string UserId)
        {
            return _ctx.Users.FirstOrDefault(x => x.Id == UserId).IsActive;
        }

        public async Task<string> FindUser(string UserName, string Password)
        {
            var user = await _userManager.FindByEmailAsync(UserName);
            if (user == null)
            {
                user = await _userManager.FindAsync(UserName, Password);
            }
            else
            {
                user = await _userManager.FindAsync(user.UserName, Password);
            }
            return user != null ? user.Id : string.Empty;
        }

        public async Task<ResponseModel<object>> FacebookLogin(UserFacebookSignInModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            var UserName = GlobalConfig.FacebookUsernamePrefix + model.FacebookId;
            var Password = model.FacebookId + GlobalConfig.FacebookPasswordSuffix;

            var data = await _userManager.FindByNameAsync(UserName);
            if (data != null)
            {
                if (data.IsActive)
                {
                    ResponseModel<TokenModel> mTokenResult = await GetUserToken(UserName, Password);
                    if (mTokenResult != null)
                    {
                        if (mTokenResult.Status == ResponseStatus.Success)
                        {
                            //Add User Device Config
                            UserLogin(data.Id, model.DeviceToken, model.Platform);

                            LoginResponseModel userSigninResponseModel = new LoginResponseModel()
                            {
                                AccessToken = mTokenResult.Result.AccessToken,
                                Error = mTokenResult.Result.Error,
                                ExpiresIn = mTokenResult.Result.ExpiresIn,
                                RefreshToken = mTokenResult.Result.RefreshToken,
                                TokenType = mTokenResult.Result.TokenType,
                                IsVerified = data.PhoneNumberConfirmed,
                                Email = data.Email,
                                Username = data.FirstName
                            };
                            mResult.Result = userSigninResponseModel;
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = string.Format("Welcome {0} to {1}", data.FirstName, GlobalConfig.ProjectName);
                        }
                        else
                        {
                            mResult.Message = mTokenResult.Message;
                        }
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "User is deactivated.";
                    mResult.Result = new { };
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "You can not registered";
                mResult.Result = new { };
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> UserLoginWithEmail(UserSignInModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            var data = await _userManager.FindByEmailAsync(model.EmailID);
            if (data != null)
            {
                if (data.IsActive)
                {
                    ResponseModel<TokenModel> mTokenResult = await GetUserToken(model.EmailID, model.Password);
                    if (mTokenResult != null)
                    {
                        if (mTokenResult.Status == ResponseStatus.Success)
                        {
                            //Add User Device Config
                            UserLogin(data.Id, model.DeviceToken, model.Platform);

                            LoginResponseModel userSigninResponseModel = new LoginResponseModel()
                            {
                                AccessToken = mTokenResult.Result.AccessToken,
                                Error = mTokenResult.Result.Error,
                                ExpiresIn = mTokenResult.Result.ExpiresIn,
                                RefreshToken = mTokenResult.Result.RefreshToken,
                                TokenType = mTokenResult.Result.TokenType,
                                IsVerified = data.PhoneNumberConfirmed,
                                Email = data.Email,
                                Username = data.FirstName
                            };

                            mResult.Result = userSigninResponseModel;
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = string.Format("Welcome {0} to {1}", data.FirstName, GlobalConfig.ProjectName);
                        }
                        else
                        {
                            mResult.Message = "Invalid password";
                            //mResult.Message = mTokenResult.Message;
                        }
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "User is deactivated";
                    mResult.Result = new { };
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "You can not registered";
                mResult.Result = new { };
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> UserSighnUp(UserSignUpModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            string UserName = string.Empty;
            string Password = string.Empty;

            if (model.IsFacebookUser)
            {
                UserName = GlobalConfig.FacebookUsernamePrefix + model.FacebookId;
                Password = model.FacebookId + GlobalConfig.FacebookPasswordSuffix;
            }
            else
            {
                UserName = model.EmailID;
                Password = model.Password;
            }

            var data = await _userManager.FindByNameAsync(UserName);
            if (data == null)
                data = await _userManager.FindByEmailAsync(model.EmailID);
            if (data == null)
            {
                data = new ApplicationUser()
                {
                    Email = model.EmailID,
                    UserName = UserName,
                    PhoneNumber = model.PhoneNumber,
                    FirstName = model.FirstName,
                    PhoneNumberConfirmed = true,
                    IsActive = true
                };

                IdentityResult result = new IdentityResult();
                try
                {
                    result = await _userManager.CreateAsync(data, Password);
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return mResult;
                }
                catch (Exception ex)
                {
                    mResult.Message = ex.Message;
                }

                if (result.Succeeded)
                {
                    //Add Role
                    var roleResult = await _userManager.AddToRoleAsync(data.Id, Role.MobileUser.ToString());

                    if (roleResult.Succeeded)
                    {
                        //Add User Device Config
                        UserLogin(data.Id, model.DeviceToken, model.Platform);

                        ResponseModel<TokenModel> mTokenResult = await GetUserToken(UserName, Password);
                        if (mTokenResult != null)
                        {
                            if (mTokenResult.Status == ResponseStatus.Success)
                            {
                                LoginResponseModel userSigninResponseModel = new LoginResponseModel()
                                {
                                    AccessToken = mTokenResult.Result.AccessToken,
                                    Error = mTokenResult.Result.Error,
                                    ExpiresIn = mTokenResult.Result.ExpiresIn,
                                    RefreshToken = mTokenResult.Result.RefreshToken,
                                    TokenType = mTokenResult.Result.TokenType,
                                    IsVerified = data.PhoneNumberConfirmed,
                                    Email = data.Email,
                                    Username = data.FirstName
                                };
                                mResult.Result = userSigninResponseModel;
                                mResult.Status = ResponseStatus.Success;
                                mResult.Message = "Congratulations! You have been successfully registered with " + GlobalConfig.ProjectName;
                            }
                            else
                            {
                                mResult.Message = mTokenResult.Message;
                            }
                        }
                    }
                    else
                    {
                        mResult.Message = string.Join("; ", result.Errors);
                    }
                }
                else
                {
                    mResult.Message = string.Join("; ", result.Errors);
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Already;
                mResult.Message = "You are already register with " + GlobalConfig.ProjectName;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> GetMyProfileForEdit(string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var profileUser = await (from user in _ctx.Users
                                     where user.Id == UserId
                                     select new
                                     {
                                         user.Email,
                                         user.FirstName,
                                         user.PhoneNumber,
                                         user.UserName,
                                     }).FirstOrDefaultAsync();
            if (profileUser != null)
            {
                MyProfileResponseModel response = new MyProfileResponseModel()
                {
                    Email = profileUser.Email,
                    FirstName = profileUser.FirstName,
                    PhoneNumber = profileUser.PhoneNumber,
                    Username = profileUser.UserName,
                };
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "";
                mResult.Result = response;
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = "Resource can't be found";
                mResult.Result = new { };
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SaveMyProfile(EditMyProfileSumbitModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            var profileUser = await (from user in _ctx.Users
                                     where user.Id == model.UserId
                                     select user)
                            .SingleOrDefaultAsync();
            if (profileUser != null)
            {
                if (_ctx.Users.Where(x => x.Email == model.EmailID).Any())
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Email already used by other user";
                    mResult.Result = new { };
                }
                else
                {
                    profileUser.FirstName = model.FirstName;
                    profileUser.Email = model.EmailID;
                    //profileUser.PhoneNumber = model.PhoneNumber;

                    await _ctx.SaveChangesAsync();

                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Profile updated";
                    mResult.Result = new { };
                }
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = "Resource can't be found";
                mResult.Result = new { };
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ChangePassword(UserChangePasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = "You are unauthorized to access this resource.";
            }
            else
            {
                IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Password changed successfully";
                }
                else
                {
                    mResult.Message = string.Join("; ", result.Errors);
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            var user = await _userManager.FindByEmailAsync(model.EmailID);
            if (user != null)
            {
                model.EmailID = user.Email;
                try
                {
                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", user.FirstName);
                    //dicPlaceholders.Add("{{password}}", password);
                    dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                    dicPlaceholders.Add("{{link}}", GlobalConfig.ForgetPasswordUrl + user.Id);

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format("Reset Password : {0}", GlobalConfig.ProjectName);
                    message.Body = Utility.GenerateEmailBody(EmailTemplate.ForgotPassword, dicPlaceholders);
                    message.Destination = user.Email;
                    await new EmailService().SendAsync(message);
                    mResult.Message = "Email Send successfully";
                    mResult.Status = ResponseStatus.Success;
                }
                catch (Exception e)
                {
                    mResult.Message = e.Message;
                    mResult.Status = ResponseStatus.Failed;
                }

            }
            else
                mResult.Message = "You are not register with this email!";
            return mResult;
        }

        public async Task<ResponseModel<object>> SetPassword(ResetPasswordModel model)
        {
            //ResponseModel<UserResponseModel> kResult = new ResponseModel<UserResponseModel>();
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user != null)
            {
                var result = await ForgotPassword(user.Email, model.NewPassword);
                if (result.Status == ResponseStatus.Success)
                {
                    mResult.Message = "Password changed successfully!";
                    mResult.Status = ResponseStatus.Success;
                }
                else
                {
                    mResult.Message = "Something went wrong!";
                    mResult.Status = ResponseStatus.Failed;
                }
            }
            else
            {
                mResult.Message = "User not found!";
                mResult.Status = ResponseStatus.Failed;
            }
            mResult.Result = model;
            return mResult;
        }

        public async Task<ResponseModel<ApplicationUser>> ForgotPassword(string userName, string password)
        {
            ResponseModel<ApplicationUser> mResult = new ResponseModel<ApplicationUser>();
            var user = await _userManager.FindByEmailAsync(userName);
            mResult = new ResponseModel<ApplicationUser>();

            if (user != null)
            {
                user.PasswordHash = _userManager.PasswordHasher.HashPassword(password);
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                    mResult.Status = ResponseStatus.Success;
                else
                {
                    mResult.Status = ResponseStatus.NotFound;
                    mResult.Message = result.Errors.ToString();
                    return mResult;
                }
            }
            else
                mResult.Message = "Resource can't be found";
            return mResult;
        }

        private void UserLogin(string UserID, string DeviceToken, Platform Platform)
        {
            UserLoginTransaction data = _ctx.UserLoginTransactions.FirstOrDefault(x => x.DeviceToken == DeviceToken);
            if (data == null)
            {
                data = new UserLoginTransaction()
                {
                    LoginDateTime = Utility.GetSystemDateTimeUTC(),
                    DeviceToken = DeviceToken,
                    Platform = Platform,
                    UserId = UserID,
                };
                _ctx.UserLoginTransactions.Add(data);
            }
            else
            {
                data.IsLogout = false;
                data.UserId = UserID;
                data.Platform = Platform;
                data.LoginDateTime = Utility.GetSystemDateTimeUTC();
            }
            _ctx.SaveChanges();
        }

        private async Task<ResponseModel<TokenModel>> GetUserToken(string UserName, string Password)
        {
            ResponseModel<TokenModel> mResult = new ResponseModel<TokenModel>();
            using (var client = new HttpClient())
            {
                List<KeyValuePair<string, string>> requestParams = new List<KeyValuePair<string, string>>
                            {
                                new KeyValuePair<string, string>("grant_type", "password"),
                                new KeyValuePair<string, string>("username", UserName),
                                new KeyValuePair<string, string>("password", Password),
                            };
                var request = HttpContext.Current.Request;
                var tokenServiceUrl = request.Url.GetLeftPart(UriPartial.Authority) + request.ApplicationPath + "/Token";
                var requestParamsFormUrlEncoded = new FormUrlEncodedContent(requestParams);
                var tokenServiceResponse = await client.PostAsync(tokenServiceUrl, requestParamsFormUrlEncoded);
                var responseString = await tokenServiceResponse.Content.ReadAsStringAsync();
                var responseCode = tokenServiceResponse.StatusCode;
                if (tokenServiceResponse.StatusCode == HttpStatusCode.OK)
                {
                    mResult.Result = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenModel>(responseString); ;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "";
                }
                else
                {
                    mResult.Message = tokenServiceResponse.RequestMessage.ToString();
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SaveContact(ContactUsRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ContactUs contactus = new ContactUs()
                {
                    UserId = model.UserId,
                    MessageText = model.MessageText,
                };
                _ctx.ContactUs.Add(contactus);
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Query submitted.";
            }
            catch (Exception ex)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> UpdatePhoneNumber(VerifyPhoneNumberModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ApplicationUser entity = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == model.UserId);

            if (entity != null)
            {
                entity.PhoneNumberConfirmed = true;
                entity.PhoneNumber = model.PhoneNumber;

                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Phone number verified";
            }
            else
            {
                mResult.Status = ResponseStatus.NotFound;
                mResult.Message = "Resource can't be found";
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
