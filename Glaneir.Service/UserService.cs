﻿using Glaneir.Core.Models;
using Glaneir.Data.Repository;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glaneir.Service
{
    public class UserService : IDisposable
    {
        private UserRepository _repo;
        private readonly IOwinContext _owin;

        public UserService()
        {
            _repo = new UserRepository();
        }

        public UserService(IOwinContext owin)
        {
            _repo = new UserRepository(owin);
            this._owin = owin;
        }
        #region Admin panel

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            return await _repo.AdminLogin(model);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            return await _repo.AdminChangePassword(model);
        }

        public IQueryable<UserDetailsModel> getAllUsers_IQueryable()
        {
            return _repo.getAllUsers_IQueryable();
        }

        public async Task<ResponseModel<object>> Delete(string UserId)
        {
            return await _repo.Delete(UserId);
        }

        public ResponseModel<bool> ActiveToggle(string userId)
        {
            return _repo.ActiveToggle(userId);
        }

        public void WebLogout()
        {
            _repo.WebLogout();
        }
        #endregion
        public bool CheckUserIsActive(string UserId)
        {
            return _repo.CheckUserIsActive(UserId);
        }

        public async Task<string> FindUser(string UserName, string Password)
        {
            return await _repo.FindUser(UserName, Password);
        }

        public async Task<ResponseModel<object>> UserSighnUp(UserSignUpModel model)
        {
            return await _repo.UserSighnUp(model);
        }

        public async Task<ResponseModel<object>> FacebookLogin(UserFacebookSignInModel model)
        {
            return await _repo.FacebookLogin(model);
        }

        public async Task<ResponseModel<object>> UserLoginWithEmail(UserSignInModel model)
        {
            return await _repo.UserLoginWithEmail(model);
        }

        public async Task<ResponseModel<object>> GetMyProfileForEdit(string UserId)
        {
            return await _repo.GetMyProfileForEdit(UserId);
        }

        public async Task<ResponseModel<object>> SaveMyProfile(EditMyProfileSumbitModel model)
        {
            return await _repo.SaveMyProfile(model);
        }

        public async Task<ResponseModel<object>> ChangePassword(UserChangePasswordModel model)
        {
            return await _repo.ChangePassword(model);
        }

        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            return await _repo.ForgotPassword(model);
        }
        public async Task<ResponseModel<object>> SaveContact(ContactUsRequestModel model)
        {
            return await _repo.SaveContact(model);
        }

        public async Task<ResponseModel<object>> SetPassword(ResetPasswordModel model)
        {
            return await _repo.SetPassword(model);
        }

        public async Task<ResponseModel<object>> UpdatePhoneNumber(VerifyPhoneNumberModel model)
        {
            return await _repo.UpdatePhoneNumber(model);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
