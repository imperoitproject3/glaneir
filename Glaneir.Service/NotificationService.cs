﻿using Glaneir.Core.Models;
using Glaneir.Data.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Glaneir.Service
{
    public class NotificationService
    {
        private NotificationRepository _repo;
        public NotificationService()
        {
            _repo = new NotificationRepository();
        }

        public async Task Add(NotificationAddModel model)
        {
            await _repo.Add(model);
        }

        public async Task<ResponseModel<List<NotificationModel>>> GetNotification(string userId)
        {
            return await _repo.GetNotification(userId);
        }
    }
}
