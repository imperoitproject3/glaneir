﻿using Glaneir.Core.Models;
using Glaneir.Data.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Glaneir.Service
{
    public class CardService
    {
        private CardRepository _repo;
        public CardService()
        {
            _repo = new CardRepository();
        }

        public async Task<ResponseModel<object>> Add(CardSaveRequestModel model)
        {
            return await _repo.Add(model);
        }

        public async Task<ResponseModel<List<CardholderIdModel>>> GetCardIdList(string UserId)
        {
            return await _repo.GetCardIdList(UserId);
        }

        public IQueryable<CardViewModel> GetUserCardList(string UserId)
        {
            return _repo.GetUserCardList(UserId);
        }

        public async Task<ResponseModel<CardDetailModel>> CardDetail(CardholderIdModel model)
        {
            return await _repo.CardDetail(model);
        }

        public async Task<ResponseModel<List<StatementsResultModel>>> GetStatement(StatementRequestModel model)
        {
            return await _repo.GetStatement(model);
        }

        public async Task<ResponseModel<ChangedStatusResponseModel>> LockUnlock(CardChangeStatusModel model)
        {
            return await _repo.LockUnlock(model);
        }

        public async Task<ResponseModel<ChangedStatusResponseModel>> ChangeStatus(CardChangeStatusModel model)
        {
            return await _repo.ChangeStatus(model);
        }

        public async Task<ResponseModel<object>> PinRequest(CardholderIdModel model)
        {
            return await _repo.PinRequest(model);
        }

        public async Task<ResponseModel<object>> AddBalance(AddBalanceRequestModel model, string userId)
        {
            return await _repo.AddBalance(model, userId);
        }

        public async Task<ResponseModel<object>> PaytoOtherCard(CardToCardRequestModel model, string userId)
        {
            return await _repo.PaytoOtherCard(model, userId);
        }
    }
}
