﻿using Glaneir.Core.Models;
using Glaneir.Service;
using System.Linq;
using System.Web.Mvc;

namespace Glaneir.Admin.Controllers
{
    public class CardController : Controller
    {
        private CardService _service;
        public CardController()
        {
            _service = new CardService();
        }

        public ActionResult ViewCardByUserId(string id)
        {
            IQueryable<CardViewModel> objCard = _service.GetUserCardList(id);
            return View(objCard);
        }
    }
}