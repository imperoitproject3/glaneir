﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Helper;
using Glaneir.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Glaneir.Admin.Controllers
{
    public class UtilityController : ApplicationBaseController
    {
        public ActionResult About()
        {
            UtilityModel model = new UtilityModel();
            try
            {
                string FilePath = GlobalConfig.fileRoutePath + "About.html";
                model.Description = System.IO.File.ReadAllText(FilePath);
            }
            catch (Exception ex)
            {
                OnBindMessage(AlertStatus.danger, ex.Message + "InnerExceptionError" + ex.InnerException);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AboutPost(UtilityModel model)
        {
            string FilePath = GlobalConfig.fileRoutePath + "About.html";
            System.IO.File.WriteAllText(FilePath, model.Description);
            OnBindMessage(AlertStatus.success, "About Us updated successfully.");
            return RedirectToAction("About");
        }
    }
}