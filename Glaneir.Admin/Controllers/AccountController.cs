﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Models;
using Glaneir.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Glaneir.Admin.Controllers
{
    public class AccountController : ApplicationBaseController
    {
        private UserService _service;

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToActionPermanent("Index", "Home");
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(AdminLoginModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());
                ResponseModel<object> mResult = await _service.AdminLogin(model);
                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToAction("Index", "Home");
                ModelState.AddModelError("", mResult.Message);
            }
            return View(model);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(AdminChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());
                ResponseModel<object> mResult = await _service.AdminChangePassword(model);

                OnBindMessage(mResult.Status, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToActionPermanent("Index", "Users");
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            _service = new UserService(Request.GetOwinContext());
            _service.WebLogout();
            return RedirectToActionPermanent("Login", "Account");
        }


    }
}