﻿using Glaneir.Core.Models;
using Glaneir.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Glaneir.Admin.Controllers
{
    public class UsersController : Controller
    {
        private UserService _service;
        public UsersController()
        {
            _service = new UserService();
        }

        public ActionResult Index()
        {
            IQueryable<UserDetailsModel> objUsers = _service.getAllUsers_IQueryable();
            return View(objUsers);
        }

        [HttpPost]
        public JsonResult ActiveToggle(string Id)
        {
            ResponseModel<bool> mResult = _service.ActiveToggle(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> Delete(string id)
        {
            ResponseModel<object> mResult = await _service.Delete(id);
            //OnBindMessage(mResult.Status, mResult.Message);
            return RedirectToAction("Index");
        }

        public ActionResult ForgetPassword(string UserId)
        {
            ViewBag.userid = UserId;
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string UserId)
        {
            ViewBag.userid = UserId;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> SetPassword(ResetPasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.SetPassword(model);
            return Json(mResult.Message, JsonRequestBehavior.AllowGet);
        }
    }
}