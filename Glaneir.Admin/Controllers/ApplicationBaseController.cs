﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Glaneir.Admin.Controllers
{
    public class ApplicationBaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (!ModelState.IsValid)
            {
                string messages = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                OnBindMessage(AlertStatus.danger, messages);
            }
            base.OnActionExecuted(filterContext);
        }

        public void OnBindMessage(ResponseStatus status, string message)
        {
            OnBindMessage((status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger), message);
        }
        public void OnBindMessage(AlertStatus status, string message)
        {
            TempData["alertModel"] = new AlertModel(status, message);
        }

        public void OnInitBreadcrumb()
        {
            string actionName = ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = ControllerContext.RouteData.Values["controller"].ToString();

            if (controllerName != "Home")
            {
                if (actionName == "Index")
                {
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        subActionTitle = controllerName
                    };
                }
                else
                {
                    ViewBag.Action = actionName;
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        actioncontroller = controllerName,
                        actionName = "Index",
                        actionTitle = controllerName,
                        subActionTitle = actionName
                    };
                }
            }
        }
    }
}