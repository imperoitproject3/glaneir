﻿namespace Glaneir.Core.Enumerations
{
    public enum Role
    {
        Admin = 1,
        MobileUser = 2
    }

    public enum ResponseStatus
    {
        Failed = 0,
        Success = 1,
        Already = 2,
        MaxAvailableSpacesFull = 3,
        Paused = 5,
        AdminBlocked = 10,
        BadRequest = 400,
        Unauthorized = 401,
        NotFound = 404,
        InternalError = 500,
        Delete = 6
    }

    public enum Platform
    {
        Android = 1,
        iOS = 2,
        Web = 3
    }

    public enum EmailTemplate
    {
        ForgotPassword,
        SendInvitation,
        FeedbackReply
    }

    public enum AlertStatus
    {
        danger = 0,
        success = 1,
        info = 2,
        warning = 3,
        primary = 4
    }

    public enum NotificationType
    {

    }

    public enum HttpMethodTypes
    {
        get,
        post,
        put,
        delete
    }

    public enum CardStatus
    {
        Issued,
        Open,
        Lost,//Block card
        Stolen,
        DepositOnly,//Lock card
        CheckReason,
        Closed,//Delete card
        A_PinChangeRequired,
        C_PhoneNumberVerification
    }

    public enum TransactionTypes
    {
        Credit,
        Debit
    }
}
