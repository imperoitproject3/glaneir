﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Glaneir.Core.Models
{
    public class NotificationAddModel
    {
        public string UserId { get; set; }
        [Required]
        public string NotificationText { get; set; }
    }

    public class NotificationModel
    {
        public string Text { get; set; }
        public string HourAgo { get; set; }
    }
}
