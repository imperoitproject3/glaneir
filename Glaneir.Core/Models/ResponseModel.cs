﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glaneir.Core.Models
{
    public class ResponseModel<T> where T : new()
    {
        public ResponseModel()
        {
            Result = new T();
            Status = ResponseStatus.Failed;
            Message = string.Empty;
        }

        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
    public class ApplicationSettingModel
    {
        public bool Maintenance { get; set; }
        public bool ForceUpdate { get; set; }
    }

    public class PaginationModel
    {
        public PaginationModel()
        {
            PageIndex = 1;
            PageSize = GlobalConfig.PageSize;
        }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class TokenModel
    {
        public TokenModel()
        {
            RefreshToken = string.Empty;
            Error = string.Empty;
            AccessToken = string.Empty;
            TokenType = string.Empty;
        }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
