﻿using Glaneir.Core.Enumerations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Glaneir.Core.Models
{
    public class UserDetailsModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsVerifyed { get; set; }
        public int NumberOFCard { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class UserFacebookSignInModel
    {
        [Display(Name = "Facebook Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string FacebookId { get; set; }

        [Display(Name = "platform")]
        [Required(ErrorMessage = "Please enter {0}")]
        public Platform Platform { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }
    }

    public class UserSignInModel
    {

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-\+]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is not valid")]
        public string EmailID { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }

        [Display(Name = "platform")]
        [Required(ErrorMessage = "Please enter {0}")]
        public Platform Platform { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }
    }

    public class UserSignUpModel
    {

        [Display(Name = "Facebook Id")]
        public string FacebookId { get; set; }

        [Required]
        public bool IsFacebookUser { get; set; }

        [Display(Name = "FirstName")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string FirstName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-\+]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is not valid")]
        public string EmailID { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }


        [Display(Name = "platform")]
        [Required(ErrorMessage = "Please enter {0}")]
        public Platform Platform { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }
    }

    public class UserSignoutModel
    {
        public string UserId { get; set; }

        [Display(Name = "device token")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string DeviceToken { get; set; }
    }

    public class LoginResponseModel : TokenModel
    {
        public bool IsVerified { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }

    public class MyProfileResponseModel
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class EditMyProfileSumbitModel
    {
        public string UserId { get; set; }

        [Display(Name = "FirstName")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string FirstName { get; set; }

        public string PhoneNumber { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-\+]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is not valid")]
        public string EmailID { get; set; }
    }

    public class UserChangePasswordModel
    {
        public string UserId { get; set; }

        [Display(Name = "current password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string CurrentPassword { get; set; }

        [Display(Name = "new password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "confirm password")]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Comfirm Password does not match with Password")]
        public string ConfirmPassword { get; set; }
    }

    public class UserForgotPasswordModel
    {
        [Display(Name = "email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [EmailAddress(ErrorMessage = "Please enter valid {0}")]
        public string EmailID { get; set; }
    }

    public class ResetPasswordModel
    {
        public string UserId { get; set; }

        [Display(Name = "email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [EmailAddress(ErrorMessage = "Please enter valid {0}")]
        public string EmailID { get; set; }

        [Display(Name = "new password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "confirm password")]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Comfirm Password does not match with Password")]
        public string ConfirmPassword { get; set; }
    }

    public class VerifyPhoneNumberModel
    {
        public string UserId { get; set; }
        [Required]
        public string FBAccountkitId { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
    }
}
