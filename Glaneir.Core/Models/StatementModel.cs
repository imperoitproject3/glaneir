﻿using Glaneir.Core.Enumerations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Glaneir.Core.Models
{
    public class StatementRequestModel
    {
        [Required(ErrorMessage = "{0} Required")]
        public string Cardholderid { get; set; }
        [Required(ErrorMessage = "{0} Required")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "{0} Required")]
        public DateTime EndDate { get; set; }
    }

    public class StatementsResultModel
    {
        public string Description { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public string TransactionType { get; set; }
    }
}
