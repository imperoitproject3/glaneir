﻿using Glaneir.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glaneir.Core.Models
{
    public class AlertModel
    {
        public AlertModel(AlertStatus alertStatus, string alertMessage)
        {
            this.alertStatus = alertStatus;
            this.alertMessage = alertMessage;
        }
        public AlertStatus alertStatus { get; set; }
        public string alertMessage { get; set; }
    }

    public class BreadcrumbModel
    {
        public string actionTitle { get; set; }
        public string actionName { get; set; }
        public string actioncontroller { get; set; }
        public string subActionTitle { get; set; }
    }

    public class UtilityModel
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }

    public class ContactUsRequestModel
    {
        public string UserId { get; set; }

        [Required]
        public string MessageText { get; set; }
    }
}
