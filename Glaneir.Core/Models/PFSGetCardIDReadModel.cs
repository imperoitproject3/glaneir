﻿namespace Glaneir.Core.Models
{
    public class PFSGetCardResultModel
    {
        public string ErrorCode { get; set; }
        public string Description { get; set; }
        public CardholderIdModel GetCardID { get; set; }
    }
}
