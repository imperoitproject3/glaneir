﻿namespace Glaneir.Core.Models
{
    public class PFSCardReadModel
    {
        public CardAccountAPIv2Model AccountAPIv2 { get; set; }
    }

    public class CardAccountAPIv2Model
    {
        public string ErrorCode { get; set; }
        public string Description { get; set; }
        public CardInquiryModel CardInquiry { get; set; }
    }

    public class CardInquiryModel
    {
        public CardInfoModel cardinfo { get; set; }
        public CardHolderModel cardholder { get; set; }
    }

    public class CardInfoModel
    {
        public string AccountBaseCurrency { get; set; }
        public string CardType { get; set; }
        public string AccountNumber { get; set; }
        public string CardStatus { get; set; }
        public string PinTriesExceeded { get; set; }
        public string BadPinTries { get; set; }
        public string ExpirationDate { get; set; }
        public object Client { get; set; }
        public object PhonecardNumber { get; set; }
        public double AvailBal { get; set; }
        public string LedgerBal { get; set; }
        public string DistributorCode { get; set; }
        public string LoadAmount { get; set; }
        public object CompanyName { get; set; }
        public string CardStyle { get; set; }
        public string DeliveryType { get; set; }
        public string SortCode { get; set; }
        public string SortCodeAccountNumber { get; set; }
    }

    public class CardHolderModel
    {
        public string FirstName { get; set; }
        public object MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public object Address2 { get; set; }
        public string City { get; set; }
        public object State { get; set; }
        public string Zip { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string DOB { get; set; }
        public object SSN { get; set; }
        public object SecurityField1 { get; set; }
        public object SecurityField2 { get; set; }
        public object UserDefinedField1 { get; set; }
        public object UserDefinedField2 { get; set; }
        public object UserDefinedField3 { get; set; }
        public object UserDefinedField4 { get; set; }
        public string EmailAddr { get; set; }
        public string CardHolderID { get; set; }
        public string CardNumber { get; set; }
        public object Address3 { get; set; }
        public object Address4 { get; set; }
        public object CountryName { get; set; }
        public object CountyName { get; set; }
        public object EmbossName { get; set; }
        public object SecurityField3 { get; set; }
        public object SecurityField4 { get; set; }
        public object SecondaryAddress1 { get; set; }
        public object SecondaryAddress2 { get; set; }
        public object SecondaryAddress3 { get; set; }
        public object SecondaryAddress4 { get; set; }
        public object City2 { get; set; }
        public object State2 { get; set; }
        public object Zip2 { get; set; }
        public object CountryCode2 { get; set; }
        public object CountryName2 { get; set; }
        public object CountyName2 { get; set; }
        public object Phone2 { get; set; }
        public object DocumentType { get; set; }
        public object DocumentNumber { get; set; }
        public object DocumentExpiryDate { get; set; }
        public object Nationality { get; set; }
        public object CountryOfIssuance { get; set; }
        public object UDFs { get; set; }
    }
}
