﻿namespace Glaneir.Core.Models
{
    public class PFSCardIssueReadModel
    {
        public CardIssueAccountAPIv2Model AccountAPIv2 { get; set; }
    }

    public class CardIssueAccountAPIv2Model
    {
        public string ErrorCode { get; set; }
        public string Description { get; set; }
        public CardIssueResultModel CardIssue { get; set; }
        public string ReferenceID { get; set; }
    }

    public class CardIssueResultModel
    {
        public string Cardholderid { get; set; }
        public string AvailableBalance { get; set; }
        public string LedgerBalance { get; set; }
    }
}
