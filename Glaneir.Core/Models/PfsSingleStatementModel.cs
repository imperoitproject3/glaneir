﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glaneir.Core.Models
{
    public partial class SinglePFSStatementReadModel
    {
        //[JsonProperty("?xml")]
        //public Xml Xml { get; set; }

        [JsonProperty("AccountAPIv2")]
        public SingleAccountApIv2 AccountApIv2 { get; set; }
    }

    public partial class SingleAccountApIv2
    {
        [JsonProperty("ErrorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("Description")]
        public object Description { get; set; }

        [JsonProperty("ViewStatement")]
        public SingleViewStatement ViewStatement { get; set; }
    }

    public partial class SingleViewStatement
    {
        [JsonProperty("cardholderstatementdetails")]
        public SingleCardholderstatementdetails Cardholderstatementdetails { get; set; }
    }

    public partial class SingleCardholderstatementdetails
    {
        [JsonProperty("@RecCnt")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long RecCnt { get; set; }

        [JsonProperty("cardpan")]
        public SingleCardpan Cardpan { get; set; }
    }

    public partial class SingleCardpan
    {
        [JsonProperty("@account")]
        public string Account { get; set; }

        [JsonProperty("@startdate")]
        public string Startdate { get; set; }

        [JsonProperty("@enddate")]
        public string Enddate { get; set; }

        [JsonProperty("@reportdate")]
        public string Reportdate { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("cardaccount")]
        public SingleCardaccount Cardaccount { get; set; }
    }

    public partial class SingleCardaccount
    {
        [JsonProperty("transactionlist")]
        public Transactionlist Transactionlist { get; set; }
    }

    public partial class ViewTransaction
    {
        [JsonProperty("transaction")]
        public SingleTransaction Transaction { get; set; }
    }

    public partial class SingleTransaction
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("Cardholderid")]
        public string Cardholderid { get; set; }

        [JsonProperty("TransactionType")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string TransactionType { get; set; }

        [JsonProperty("MTI")]
        public string Mti { get; set; }

        [JsonProperty("ARN")]
        public string Arn { get; set; }

        [JsonProperty("STN")]
        public string Stn { get; set; }

        [JsonProperty("TermID")]
        public object TermId { get; set; }

        [JsonProperty("AuthNum")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long AuthNum { get; set; }

        [JsonProperty("RecType")]
        public object RecType { get; set; }

        [JsonProperty("TransactionOrigin")]
        public string TransactionOrigin { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("fee")]
        public string Fee { get; set; }

        [JsonProperty("availablebalance")]
        public string Availablebalance { get; set; }

        [JsonProperty("ledgerbalance")]
        public string Ledgerbalance { get; set; }

        [JsonProperty("issuerfee")]
        public string Issuerfee { get; set; }

        [JsonProperty("clientid")]
        public string Clientid { get; set; }

        [JsonProperty("termnamelocation")]
        public string Termnamelocation { get; set; }

        [JsonProperty("termowner")]
        public object Termowner { get; set; }

        [JsonProperty("termcity")]
        public object Termcity { get; set; }

        [JsonProperty("termstate")]
        public object Termstate { get; set; }

        [JsonProperty("termcountry")]
        public object Termcountry { get; set; }

        [JsonProperty("surcharge")]
        public string Surcharge { get; set; }

        [JsonProperty("rspcode")]
        public string Rspcode { get; set; }

        [JsonProperty("mcc")]
        public object Mcc { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("origholdamt")]
        public string Origholdamt { get; set; }

        [JsonProperty("termcurrency")]
        public string Termcurrency { get; set; }

        [JsonProperty("origtransamt")]
        public string Origtransamt { get; set; }
    }
}
