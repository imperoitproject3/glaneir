﻿using Glaneir.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Glaneir.Core.Models
{
    public class CardViewModel
    {
        public string UserName { get; set; }
        public string CardName { get; set; }
        public string CardNumber { get; set; }
        public DateTime CreatedDateTimeUTC { get; set; }
    }

    public class CardSaveRequestModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        public string CardName { get; set; }

        [CreditCard(ErrorMessage = "Invalid {0}")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string CardNumber { get; set; }
    }

    public class CardholderIdModel
    {
        [Required(ErrorMessage = "{0} required")]
        public string Cardholderid { get; set; }
    }

    public class CardChangeStatusModel : CardholderIdModel
    {
        [Required(ErrorMessage = "{0} required")]
        public CardStatus OldStatus { get; set; }
        public CardStatus NewStatus { get; set; }
    }

    public class ChangedStatusResponseModel
    {
        public CardStatus CardStatus { get; set; }
    }

    public class CardDetailModel
    {
        public CardDetailModel()
        {
            TransactionList = new List<StatementsResultModel>();
        }
        public string Cardholderid { get; set; }
        public string CardType { get; set; }
        public CardStatus CardStatus { get; set; }
        public string Cardname { get; set; }
        public string CardNumber { get; set; }
        public string Expirydate { get; set; }
        public double AvailableBalance { get; set; }
        public List<StatementsResultModel> TransactionList { get; set; }
    }

    public class AddBalanceRequestModel
    {
        [Required(ErrorMessage = "{0} required")]
        public string Cardholderid { get; set; }

        [Required(ErrorMessage = "{0} required")]
        public double Amount { get; set; }
        //TODo: 30 charactor validation
        [MaxLength(30)]
        [Required(ErrorMessage = "{0} required")]
        public string Description { get; set; }
    }

    public class CardToCardRequestModel
    {
        [Required(ErrorMessage = "{0} required")]
        public string Cardholderid { get; set; }
        [Range(500, double.MaxValue)]
        [Required(ErrorMessage = "{0} required")]
        public double Amount { get; set; }
        [Required(ErrorMessage = "{0} required")]
        public string CardNumber { get; set; }
        [Required(ErrorMessage = "{0} required")]
        public string Description { get; set; }
    }
}

