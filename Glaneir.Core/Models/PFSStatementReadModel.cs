﻿using Glaneir.Core.Enumerations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Glaneir.Core.Models
{
    public class PFSStatementReadModel
    {
        [JsonProperty("AccountAPIv2")]
        public AccountApIv2 AccountApIv2 { get; set; }
    }

    public class AccountApIv2
    {

        [JsonProperty("ErrorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("Description")]
        public object Description { get; set; }

        [JsonProperty("ViewStatement")]
        public ViewStatement ViewStatement { get; set; }
    }

    public class ViewStatement
    {
        [JsonProperty("cardholderstatementdetails")]
        public Cardholderstatementdetails Cardholderstatementdetails { get; set; }
    }

    public class Cardholderstatementdetails
    {
        [JsonProperty("@RecCnt")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long RecCnt { get; set; }

        [JsonProperty("cardpan")]
        public Cardpan Cardpan { get; set; }
    }

    public class Cardpan
    {
        [JsonProperty("@account")]
        public string Account { get; set; }

        [JsonProperty("@startdate")]
        public string Startdate { get; set; }

        [JsonProperty("@enddate")]
        public string Enddate { get; set; }

        [JsonProperty("@reportdate")]
        public string Reportdate { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("cardaccount")]
        public Cardaccount[] Cardaccount { get; set; }
    }

    public class Cardaccount
    {
        [JsonProperty("transactionlist")]
        public Transactionlist Transactionlist { get; set; }
    }

    public class Transactionlist
    {
        [JsonProperty("transaction")]
        public Dictionary<string, string> Transaction { get; set; }
    }

    public class Transaction
    {
        public DateTime date { get; set; }
        public string Cardholderid { get; set; }
        public string TransactionType { get; set; }
        public string MTI { get; set; }
        public string ARN { get; set; }
        public string STN { get; set; }
        public object TermID { get; set; }
        public string AuthNum { get; set; }
        public object RecType { get; set; }
        public string TransactionOrigin { get; set; }
        public string description { get; set; }
        public double amount { get; set; }
        public string fee { get; set; }
        public string availablebalance { get; set; }
        public string ledgerbalance { get; set; }
        public string issuerfee { get; set; }
        public string clientid { get; set; }
        public string termnamelocation { get; set; }
        public string termowner { get; set; }
        public string termcity { get; set; }
        public object termstate { get; set; }
        public string termcountry { get; set; }
        public string surcharge { get; set; }
        public string rspcode { get; set; }
        public object mcc { get; set; }
        public string currency { get; set; }
        public string origholdamt { get; set; }
        public string termcurrency { get; set; }
        public string origtransamt { get; set; }
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}

