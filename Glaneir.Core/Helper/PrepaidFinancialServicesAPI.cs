﻿using Glaneir.Core.Enumerations;
using Glaneir.Core.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Glaneir.Core.Helper
{
    public class PfsAPIRequestModel
    {
        public HttpMethodTypes httpMethodType { get; set; }
        public string apiSigniture { get; set; }
        public string xmlData { get; set; }
    }

    public class PfsAPIResponseModel
    {
        public PfsApiAccountApIv2 AccountApIv2 { get; set; }
    }

    public class PfsApiAccountApIv2
    {
        public long ErrorCode { get; set; }

        public string Description { get; set; }

        public string ReferenceId { get; set; }
    }

    public class PfsResultModel
    {
        public string jsonString { get; set; }
    }

    public static class PrepaidFinancialServicesAPI
    {
        public static async Task<ResponseModel<PfsResultModel>> ApiService(PfsAPIRequestModel model)
        {
            ResponseModel<PfsResultModel> mResult = new ResponseModel<PfsResultModel>()
            {
                Status = ResponseStatus.Success
            };
            using (HttpClient client = new HttpClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                string apiHost = "https://staging.prepaidfinancialservices.com/accountapiv2/service.asmx/Process";

                Dictionary<string, string> dict = new Dictionary<string, string>
                    {
                        { "Username", GlobalConfig.PFSApiUsername },
                        { "Password", GlobalConfig.PFSApiPassword },
                        { "APISigniture", model.apiSigniture },
                        { "MessageID", Guid.NewGuid().ToString()},
                        { "Data", model.xmlData }
                    };

                try
                {
                    HttpRequestMessage req = null;
                    switch (model.httpMethodType)
                    {
                        case HttpMethodTypes.get:
                            req = new HttpRequestMessage(HttpMethod.Get, apiHost)
                            {
                                Content = new FormUrlEncodedContent(dict)
                            };
                            break;
                        case HttpMethodTypes.post:
                            req = new HttpRequestMessage(HttpMethod.Post, apiHost)
                            {
                                Content = new FormUrlEncodedContent(dict)
                            };
                            break;
                        case HttpMethodTypes.put:
                            req = new HttpRequestMessage(HttpMethod.Put, apiHost)
                            {
                                Content = new FormUrlEncodedContent(dict)
                            };
                            break;
                        case HttpMethodTypes.delete:
                            req = new HttpRequestMessage(HttpMethod.Delete, apiHost)
                            {
                                Content = new FormUrlEncodedContent(dict)
                            };
                            break;
                        default:
                            break;
                    }

                    if (req != null)
                    {
                        HttpResponseMessage res = await client.SendAsync(req);
                        string resultContent = await res.Content.ReadAsStringAsync();

                        XElement xElement = XElement.Parse(resultContent);

                        string json = JsonConvert.SerializeXNode(xElement, Newtonsoft.Json.Formatting.None, true);

                        //remove this from json {"@xmlns":"http://accountapiv2.internal/","#text":""}
                        string result = json.Replace("\\", "");
                        result = result.Substring(51);
                        result = result.TrimEnd('}');
                        result = result.TrimEnd('"');

                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(result);

                        mResult.Result.jsonString = JsonConvert.SerializeXmlNode(doc);

                        PfsAPIResponseModel PfsResponse = JsonConvert.DeserializeObject<PfsAPIResponseModel>(mResult.Result.jsonString);

                        if (PfsResponse.AccountApIv2.ErrorCode != 0000)//failed
                        {
                            mResult.Status = ResponseStatus.Failed;
                            mResult.Message = PfsResponse.AccountApIv2.Description;
                        }
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Please provide http method type.";
                    }
                }
                catch (Exception err)
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = $"Message {err.Message} InnerException{ err.InnerException}";
                    mResult.Result.jsonString = JsonConvert.SerializeObject(err);
                }                
            }
            return mResult;
        }
    }
}
