﻿using System.Configuration;

namespace Glaneir.Core.Helper
{
    public static class GlobalConfig
    {
        public const string ProjectName = "Glaneir";
        public const string DateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        public const string DisplayDateOnlyFormat = "MM/dd/yyyy";
        public const string FacebookUsernamePrefix = "FB";
        public const string FacebookPasswordSuffix = "Glaneir";

        #region PFS Credentials
        public const string PFSApiUsername = "BomadIntextapiusr";
        public const string PFSApiPassword = "R5PzBioaILb2";
        #endregion

        #region Path and URL
        public static string baseUrl = ConfigurationManager.AppSettings["BaseRouteUrl"].ToString();
        public static string baseRoutePath = ConfigurationManager.AppSettings["BaseRoutePath"];

        public static string fileRoutePath = baseRoutePath + @"Files\";
        public static string fileBaseUrl = baseUrl + "Files/";

        public static string EmailTemplatePath = fileRoutePath + @"EmailTemplate\";
        public static string EmailTemplateUrl = fileBaseUrl + "EmailTemplate/";

        public static string ForgetPasswordUrl = baseUrl + "Admin/Account/ResetPassword/?UserId=";


        #endregion

        public const int MaxAvailableSpaces = 50;

        #region Pagination
        public const int PageSize = 20;
        public const int NotificationPageSize = 1000;
        #endregion

        #region Email
        public static string EmailUserName = ConfigurationManager.AppSettings["EmailUserName"].ToString();
        public static string EmailPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
        public static string SMTPClient = ConfigurationManager.AppSettings["SMTPClient"].ToString();
        #endregion

        #region Notification
        public static string FirebaseLegacyServerKey = ConfigurationManager.AppSettings["FirebaseLegacyServerKey"].ToString();
        public static string FirebaseServerKey = ConfigurationManager.AppSettings["FirebaseServerKey"].ToString();
        public static string FirebaseProjectID = ConfigurationManager.AppSettings["FirebaseProjectID"].ToString();
        #endregion
    }
}
