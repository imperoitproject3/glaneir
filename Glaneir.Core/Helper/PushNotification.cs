﻿using Newtonsoft.Json;
using Glaneir.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Glaneir.Core.Enumerations;

namespace Glaneir.Core.Helper
{
    public class NotificationPayload
    {
        public NotificationPayload()
        {
            content_available = 0;
        }

        /// <summary>
        /// The array must contain at least 1 and at most 1000 registration tokens.
        /// </summary>
        public string[] registration_ids { get; set; }

        /// <summary>
        /// When a notification is sent and this is set to true, an inactive client app is awoken.
        /// </summary>
        public int content_available { get; set; }

        /// <summary>
        /// This parameter specifies the predefined, user-visible key-value pairs of the notification payload.
        /// </summary>
        public NotificationOptionModel notification { get; set; }

        /// <summary>
        /// This parameter specifies the custom key-value pairs of the message's payload.
        /// </summary>
        public CustomDataModel data { get; set; }
    }

    public class NotificationOptionModel
    {
        public NotificationOptionModel()
        {
            title = GlobalConfig.ProjectName;
            sound = "default";
            badge = 0;
            icon = "ic_notification_icon";
            body = string.Empty;
        }

        /// <summary>
        /// The notification's title. **This field is not visible on iOS phones and tablets.
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// The notification's body text.
        /// </summary>
        public string body { get; set; }

        /// <summary>
        /// The sound to play when the device receives the notification.
        /// </summary>
        public string sound { get; set; }

        /// <summary>
        /// The value of the badge on the home screen app icon.
        /// If not specified, the badge is not changed.
        /// If set to 0, the badge is removed.
        /// </summary>
        public int badge { get; set; }

        /// <summary>
        /// The notification's icon.
        /// Sets the notification icon to myicon for drawable resource myicon. If you don't send this key in the request, FCM displays the launcher icon specified in your app manifest.
        /// </summary>
        public string icon { get; set; }
    }

    public class CustomDataModel
    {
        public int primaryId { get; set; }
    }

    public class NotificationErrorModel
    {
        public string error { get; set; }
    }

    public class NotificationResultModel
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<NotificationErrorModel> results { get; set; }
    }

    public class PushNotification
    {
        public async Task<ResponseModel<List<NotificationResultModel>>> SendNotification(NotificationPayload model)
        {
            ResponseModel<List<NotificationResultModel>> mResult = new ResponseModel<List<NotificationResultModel>>()
            {
                Status = ResponseStatus.Success,
                Message = $"Notificatin has been sent to {model.registration_ids.Count()} users."
            };

            using (HttpClient httpClient = new HttpClient())
            {
                Uri uri = new Uri("https://fcm.googleapis.com/fcm/");
                httpClient.BaseAddress = uri;
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("Sender", $"id={ GlobalConfig.FirebaseProjectID}");
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("key", $"={GlobalConfig.FirebaseLegacyServerKey}");

                string[] deviceTokenList = model.registration_ids.GroupBy(x => x).Select(x => x.Key).ToArray();
                const int pageSize = 1000;
                int currentPage = 0;
                for (int i = 0; i < deviceTokenList.Count(); i = i + pageSize)
                {
                    model.registration_ids = deviceTokenList.Skip(currentPage * pageSize).Take(pageSize).ToArray();
                    string jsonResult = JsonConvert.SerializeObject(model);
                    jsonResult = jsonResult.Replace("content_available", "content-available");
                    HttpRequestMessage request = new HttpRequestMessage()
                    {
                        Content = new StringContent(jsonResult, Encoding.UTF8, "application/json")
                    };

                    try
                    {
                        HttpResponseMessage httpResponseMessage = await httpClient.PostAsync("send", request.Content);
                        string resultResponse = await httpResponseMessage.Content.ReadAsStringAsync();
                        mResult.Result.Add(JsonConvert.DeserializeObject<NotificationResultModel>(resultResponse));
                    }
                    catch (Exception err)
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = err.Message;
                    }
                    currentPage++;
                }
            }
            return mResult;
        }
    }
}